$(document).ready(init)

/* Media Query match for operations on the DOM based on the device */
var mobile = !window.matchMedia("(min-width: 768px)").matches

/* Document Initialization */
function init() {

  /* Init carousel with random slide displayed */
  var myCarousel = document.querySelector('#carouselSlider')
  var carousel = new bootstrap.Carousel(myCarousel)
  var slides = myCarousel.querySelectorAll(".carousel-item")
  var randN = Math.floor((Math.random() * slides.length))
  slides[randN].classList.add("active")

  /* Generate articles */
  var article = document.querySelector(".card")
  var clonedArticle
  var parent = article.parentNode
  for (var i = 0; i < 14; i++) {
    clonedArticle = article.cloneNode(true)
    clonedArticle.querySelector(".card-title").innerText = "Article " + (i+2)
    /* 2 o 3 articles for page according to the device width */
    if (i == 1 && mobile) clonedArticle.classList.add("d-none")
    if (i > 1) {
      clonedArticle.classList.add("d-none")
    }
    parent.append(clonedArticle)
  }

  /* Generate Sponsors Grid */
  var grid = document.querySelector("#sponsorGrid")
  var gridElem = grid.firstElementChild
  for (var i = 0; i < 23; i++) {
    grid.append(gridElem.cloneNode(true))
  }

  /* Generate Pagination items */
  var pagesNum = mobile ? 8 : 5
  var pageItem = document.querySelector(".page-item").nextElementSibling
  var pagination = pageItem.parentNode
  var clonedPageItem
  for (var i = pagesNum; i > 1; i--) {
    clonedPageItem = pageItem.cloneNode(true)
    clonedPageItem.classList.remove("active")
    clonedPageItem.firstElementChild.innerHTML = i
    pageItem.after(clonedPageItem)
  }

  /* Generate Sponsors Grid */
  grid = document.querySelector("#flickrGrid")
  gridElem = grid.firstElementChild
  for (var i = 0; i < 8; i++) {
    grid.append(gridElem.cloneNode(true))
  }
}

/* Manages the disabled characteristic of the angular quotes of the pagination */
function checkDisable(oldItem, newItem) {

  /* The previous and the next sibling of the item corresponding to the old page (where I come from) */
  var oldPrev = oldItem.previousElementSibling
  var oldNext = oldItem.nextElementSibling
  /* The previous and the next sibling of the item corresponding to the new page (clicked) */
  var newPrev = newItem.previousElementSibling
  var newNext = newItem.nextElementSibling

  /* If the previous or next sibling is a quote (parseInt -> false) and it's disabled, make it clickable */
  if (!parseInt(oldPrev.innerText) && oldPrev.classList.contains("disabled")) {
    oldPrev.classList.remove("disabled")
  }
  if (!parseInt(oldNext.innerText) && oldNext.classList.contains("disabled")) {
    oldNext.classList.remove("disabled")
  }

  /* If the previous or next sibling is a quote (parseInt -> false) make it disabled */
  if (!parseInt(newPrev.innerText)) {
    newPrev.classList.add("disabled")
  }
  if (!parseInt(newNext.innerText)) {
    newNext.classList.add("disabled")
  }
}

/* Pagination management */
function paginate(item, quote) {

  /* item is the anchor element, we need the list item (parent) */
  var liItem = item.parentNode

  /* Active item (that is not disabled) */
  if(!liItem.classList.contains("active")) {
    /* Pagination visual update (active class on the item corresponding to the current page) */
    var activeItem = $(liItem).siblings(".active")[0]
    if (activeItem) activeItem.classList.remove("active")
    var toActive = liItem
    if (quote == 'r')      toActive = activeItem.nextElementSibling
    else if (quote == 'l') toActive = activeItem.previousElementSibling
    if (parseInt(toActive.innerText)) toActive.classList.add("active")
    checkDisable(activeItem, toActive)

    /* Page Switch */
    var articles = document.querySelectorAll(".card")
    var n = mobile ? 2 : 3
    var oldPageNum = activeItem.innerText
    var oldPageOffset = n * (oldPageNum-1)
    var newPageNum = toActive.innerText
    var newPageOffset = n * (newPageNum-1)

    articles.forEach((article, i) => {
      /* n Articles to hide */
      if (oldPageOffset <= i && i < oldPageOffset + n) {
        article.classList.add("d-none")
      }
      /* n Articles to show */
      if (newPageOffset <= i && i < newPageOffset + n) {
        article.classList.remove("d-none")
      }
    })
  }
}

/* Simple Read More button functionality */
function readMore(button) {
  var par = button.previousElementSibling
  if (par.classList.contains("text-truncate"))
    button.innerHTML = "Read Less"
  else
    button.innerHTML = "Read More"
  par.classList.toggle("text-truncate")
}

/* Search functionality with popover alert */
function searchPage(btn) {
  var searchForm = btn.nextElementSibling
  /* window.find searches the query in the content page (no hidden articles) */
  if(!window.find(searchForm.value)) {
    var popover = new bootstrap.Popover(searchForm, {content: "Not found!", placement: "left"})
    popover.show()
    searchForm.parentNode.addEventListener('focusout', (event) => {
      popover.hide()
      popover.disable()
    })
  }
  searchForm.value = ""
}
